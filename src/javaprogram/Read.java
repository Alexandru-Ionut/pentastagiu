/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaprogram;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author Ionut
 */
public class Read {
    File file;
   public Read(File f){
       file=f;
   }
   public String Reading()
   {
       String content=" ";
       try{
        Scanner reader=new Scanner(file);
        while(reader.hasNext())
        {
            content+=reader.nextLine()+'\n';
           
        }
        reader.close();
        }catch(IOException e)
        {
            System.out.print(e.toString());
                
         
        }
       return content;
   }
   
}
