/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaprogram;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author Plosnita Alexandru-Ionut 
 */
public class Writer {
     String content;
     public Writer(String content){
         this.content=content;
     }
         public void Writing(){
             try{
             PrintWriter writer=new PrintWriter("out.txt");
             writer.print(content);
             writer.close();
             }catch(IOException e)
             {
                 System.out.print(e.toString());
                 System.out.print("Could not open file");
             }
         }
  }

